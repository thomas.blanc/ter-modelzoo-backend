package mozen.web;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mozen.business.ICommentManager;
import mozen.business.IUserManager;
import mozen.messages.CommentMessage;
import mozen.messages.ResponseMessage;
import mozen.model.Comment;
import mozen.model.User;
import mozen.utils.UserHelper;

@RestController
@RequestMapping("/api/comments")
@CrossOrigin
public class CommentController {
  @Autowired
  ICommentManager commentManager;

  @Autowired
  IUserManager userManager;

  @GetMapping("/list")
  public Collection<Comment> getAllComments() {
    return commentManager.getComments();
  }

  @GetMapping("/user")
  public ResponseEntity<?> getUserComments(@RequestParam(value = "id", required = true) Long id) {
    try {
      return ResponseEntity.ok(commentManager.getCommentFromUser(id));
    } catch (Exception e) {
      return ResponseEntity.badRequest().build();
    }
  }

  @GetMapping("")
  public ResponseEntity<?> getModelComments(@RequestParam(value = "id", required = true) Long id) {
    try {
      return ResponseEntity.ok(commentManager.getCommentFromModel(id));
    } catch (Exception e) {
      return ResponseEntity.badRequest().build();
    }
  }  

  @PostMapping("")
  public ResponseEntity<ResponseMessage> addComment(@RequestBody CommentMessage message) {
    User user = UserHelper.getCurrentUser(userManager);
    ResponseMessage response = new ResponseMessage(false, "");

    try {
      commentManager.addComment(message, user);
      return ResponseEntity.ok(response);
    } catch (Exception e) {
      response.setError(true);
      response.setMessage(e.getMessage());
      return ResponseEntity.ok(response);
    }
  }

  @DeleteMapping("")
  public ResponseEntity<ResponseMessage> deleteComment(@RequestParam(value = "id", required = true) Long id) {
    User user = UserHelper.getCurrentUser(userManager);
    ResponseMessage response = new ResponseMessage(false, "");

    try {
      commentManager.removeComment(id, user);
      return ResponseEntity.ok(response);
    } catch (Exception e) {
      response.setError(true);
      response.setMessage(e.getMessage());
      return ResponseEntity.ok(response);
    }
  }
}