package mozen.web;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mozen.business.IModelManager;
import mozen.messages.SearchResult;

@RestController
@RequestMapping("/api/search")
@CrossOrigin
public class SearchController {
  @Autowired
  IModelManager modelManager;
  
  @GetMapping("")
  public SearchResult search(
      @RequestParam(value = "name", required = false, defaultValue = "") String name,
      @RequestParam(value = "tags", required = false) Collection<String> tags,
      @RequestParam(value = "param", required = false) Integer param,
      @RequestParam(value = "size", required = true) Integer size,
      @RequestParam(value = "page", required = true) Integer page,
      @RequestParam(value = "sort", required = false, defaultValue = "votes") String sort
    ) 
  {
    SearchResult result;

    if (tags.isEmpty()) result = modelManager.findModel(name, page, size, sort, null);
    else result = modelManager.findModel(name, page, size, sort, tags);
   
    return result;
  }
}