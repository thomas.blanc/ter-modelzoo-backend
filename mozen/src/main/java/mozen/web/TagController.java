package mozen.web;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mozen.business.ITagManager;
import mozen.messages.ResponseMessage;
import mozen.messages.TagCategoryMessage;
import mozen.messages.TagMessage;
import mozen.model.TagCategory;

@RestController
@RequestMapping("/api/tags")
@CrossOrigin
public class TagController {
  @Autowired
  ITagManager tagManager;

  @GetMapping("")
  public Collection<TagCategory> getTags() {
    return tagManager.getTags();
  }

  @PostMapping("")
  public ResponseEntity<ResponseMessage> addTag(@RequestBody @Valid TagMessage message) {
    tagManager.addTag(message);
    return ResponseEntity.ok().build();
  }

  @DeleteMapping("")
  public ResponseEntity<ResponseMessage> deleteTag(@RequestParam(value = "id", required = true) Long id) {
    tagManager.removeTag(id);
    return ResponseEntity.ok().build();
  }

  @PostMapping("/category")
  public ResponseEntity<ResponseMessage> addCategory(@RequestBody @Valid TagCategoryMessage message) {
    tagManager.addCategory(message);
    return ResponseEntity.ok().build();
  }

  @DeleteMapping("/category")
  public ResponseEntity<ResponseMessage> deleteCategory(@RequestParam(value = "id", required = true) Long id) {
    tagManager.removeCategory(id);
    return ResponseEntity.ok().build();
  }
}