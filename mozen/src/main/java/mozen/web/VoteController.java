package mozen.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mozen.business.IUserManager;
import mozen.business.IVoteManager;
import mozen.messages.ResponseMessage;
import mozen.messages.VoteStatusMessage;
import mozen.model.User;
import mozen.utils.UserHelper;

import org.springframework.web.bind.annotation.RequestParam;

@RestController
@RequestMapping("/api/vote")
@CrossOrigin
public class VoteController {
  @Autowired
  IVoteManager voteManager;

  @Autowired
  IUserManager userManager;

  @GetMapping
  public ResponseEntity<?> getVoteStatus(@RequestParam(value = "id", required = true) Long id) {
    ResponseMessage response = new ResponseMessage(false, "");
    User user = UserHelper.getCurrentUser(userManager);
    if(user == null) {
      response.setError(true);
      response.setMessage("User unknown");
      return ResponseEntity.badRequest().body(response);
    }

    try {
      String status = voteManager.getStatus(id, user);
      return ResponseEntity.ok(new VoteStatusMessage(status));
    } catch (Exception e) {
      response.setError(true);
      response.setMessage(e.getMessage());
      return ResponseEntity.badRequest().body(response);
    }
  }

  @PutMapping
  public ResponseEntity<?> addVote(@RequestParam(value = "id", required = true) Long id, @RequestParam(value = "isDown", required = true) boolean isDown) {
    ResponseMessage response = new ResponseMessage(false, "");
    User user = UserHelper.getCurrentUser(userManager);
    if(user == null) {
      response.setError(true);
      response.setMessage("User unknown");
      return ResponseEntity.badRequest().body(response);
    }

    try {
      if (isDown) {
        voteManager.addDownvote(id, user);
      } else {
        voteManager.addUpvote(id, user);
      }
    } catch (Exception e) {
      response.setError(true);
      response.setMessage(e.getMessage());
      return ResponseEntity.badRequest().body(response);
    }

    return ResponseEntity.ok(response);
  }

  @DeleteMapping
  public ResponseEntity<?> removeVote(@RequestParam(value = "id", required = true) Long id, @RequestParam(value = "isDown", required = true) boolean isDown) {
    ResponseMessage response = new ResponseMessage(false, "");
    User user = UserHelper.getCurrentUser(userManager);
    if(user == null) {
      response.setError(true);
      response.setMessage("User unknown");
      return ResponseEntity.badRequest().body(response);
    }

    try {
      if (isDown) {
        voteManager.removeDownvote(id, user);
      } else {
        voteManager.removeUpvote(id, user);
      }
    } catch (Exception e) {
      response.setError(true);
      response.setMessage(e.getMessage());
      return ResponseEntity.badRequest().body(response);
    }

    return ResponseEntity.ok(response);
  }

  @GetMapping("/likedModels")
  public ResponseEntity<?> getLikedModels() {
    ResponseMessage response = new ResponseMessage(false, "");
    User user = UserHelper.getCurrentUser(userManager);
    if(user == null) {
      response.setError(true);
      response.setMessage("User unknown");
      return ResponseEntity.badRequest().body(response);
    }
    return ResponseEntity.ok(user.getUpvotes());
  }
}