package mozen.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import mozen.business.ILayerManager;
import mozen.business.IUserManager;
import mozen.model.CustomLayer;
import mozen.messages.ResponseMessage;
import mozen.model.User;
import mozen.utils.UserHelper;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RestController
@RequestMapping("/api/layers")
@CrossOrigin
public class CustomLayerController {
  @Autowired
  ILayerManager layerManager;

  @Autowired
  IUserManager userManager;

  @GetMapping("/download")
  public ResponseEntity<?> downloadLayerFile(@RequestParam(value = "id", required = true) Long id) {
    ResponseMessage response = new ResponseMessage(false, "");
  
    try {
      CustomLayer layer = layerManager.getLayer(id);
      return ResponseEntity.ok()
        .contentType(MediaType.parseMediaType(layer.getFileType()))
        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + layer.getName() + "\"")
        .body(new ByteArrayResource(layer.getFile()));
    } catch (Exception e) {
      response.setError(true);
      response.setMessage(e.getMessage());
      return ResponseEntity.badRequest().body(response);
    }
  }

  @PostMapping("/upload")
  public ResponseEntity<ResponseMessage> uploadLayerFile(
    @RequestParam("file") MultipartFile file, 
    @RequestParam(value = "id", required = true) Long id,
    @RequestParam(value = "name", required = true) String name) {
    ResponseMessage response = new ResponseMessage(false, "");
    User user = UserHelper.getCurrentUser(userManager);
    if(user == null) {
      response.setError(true);
      response.setMessage("User unknown");
      return ResponseEntity.badRequest().body(response);
    }
  
    try {
      System.err.println("[MODEL CONTROLLER] layer file upload f:"+file.getName()+" n:"+name);
      layerManager.addLayerFile(file, id, name, user);
      return ResponseEntity.ok(response);
    } catch (Exception e) {
      response.setError(true);
      response.setMessage(e.getMessage());
      return ResponseEntity.badRequest().body(response);
    }
  }
}