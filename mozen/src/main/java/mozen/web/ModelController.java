package mozen.web;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import mozen.business.IModelManager;
import mozen.business.IUserManager;
import mozen.model.Model;
import mozen.messages.ModelMessage;
import mozen.messages.ResponseMessage;
import mozen.model.User;
import mozen.utils.UserHelper;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@RestController
@RequestMapping("/api/models")
@CrossOrigin
public class ModelController {
  @Autowired
  IModelManager modelManager;

  @Autowired
  IUserManager userManager;

  @GetMapping("")
  public ResponseEntity<?> getModelDetails(@RequestParam(value = "id", required = false) Long id) {
    User user = UserHelper.getCurrentUser(userManager);
    if(user != null) {
      return ResponseEntity.ok().body(user.getModels());
    } else {
      if(id == null) return ResponseEntity.badRequest().build();
      return ResponseEntity.ok().body(modelManager.getModel(id));
    }
  }

  @PostMapping("")
  public ResponseEntity<ResponseMessage> addModel(@RequestBody @Valid ModelMessage message) {
    ResponseMessage response = new ResponseMessage(false, "");
    User user = UserHelper.getCurrentUser(userManager);
    if(user == null) {
      response.setError(true);
      response.setMessage("User unknown");
      return ResponseEntity.badRequest().body(response);
    }

    try {
      long modelId = modelManager.addModel(message, user).getId();
      response.setMessage(Long.toString(modelId));
      return ResponseEntity.ok(response);
    } catch (Exception e) {
      response.setError(true);
      response.setMessage(e.getMessage());
      return ResponseEntity.badRequest().body(response);
    }
  }

  @PutMapping("")
  public ResponseEntity<ResponseMessage> updateModel(@RequestParam(value = "id", required = true) Long id, @RequestBody @Valid ModelMessage message) {
    ResponseMessage response = new ResponseMessage(false, "");
    User user = UserHelper.getCurrentUser(userManager);
    if(user == null) {
      response.setError(true);
      response.setMessage("User unknown");
      return ResponseEntity.badRequest().body(response);
    }

    try {
      modelManager.updateModel(message, id, user);
      return ResponseEntity.ok(response);
    } catch (Exception e) {
      response.setError(true);
      response.setMessage(e.getMessage());
      return ResponseEntity.badRequest().body(response);
    }
  }

  @DeleteMapping("")
  public ResponseEntity<ResponseMessage> deleteModel(@RequestParam(value = "id", required = true) Long id) {
    ResponseMessage response = new ResponseMessage(false, "");
    User user = UserHelper.getCurrentUser(userManager);
    if(user == null) {
      response.setError(true);
      response.setMessage("User unknown");
      return ResponseEntity.badRequest().body(response);
    }

    try {
      modelManager.removeModel(id, user);
      return ResponseEntity.ok(response);
    } catch (Exception e) {
      response.setError(true);
      response.setMessage(e.getMessage());
      return ResponseEntity.badRequest().body(response);
    }
  }

  @GetMapping("/setVerified")
  public ResponseEntity<ResponseMessage> setVerified(@RequestParam(value = "id", required = true) Long id) {
    ResponseMessage response = new ResponseMessage(false, "");
    User user = UserHelper.getCurrentUser(userManager);
    if(user == null) {
      response.setError(true);
      response.setMessage("User unknown");
      return ResponseEntity.badRequest().body(response);
    }

    try {
      modelManager.setVerified(id, user);
      return ResponseEntity.ok(response);
    } catch (Exception e) {
      response.setError(true);
      response.setMessage(e.getMessage());
      return ResponseEntity.badRequest().body(response);
    }
  }

  @GetMapping("/list")
  public Collection<Model> getAllModels() {
    return modelManager.getModels();
  }

  @PostMapping("/upload")
  public ResponseEntity<ResponseMessage> uploadModelFile(@RequestParam("file") MultipartFile file, @RequestParam(value = "id", required = true) Long id) {
    ResponseMessage response = new ResponseMessage(false, "");
    User user = UserHelper.getCurrentUser(userManager);
    if(user == null) {
      response.setError(true);
      response.setMessage("User unknown");
      return ResponseEntity.badRequest().body(response);
    }
  
    try {
      System.err.println("[MODEL CONTROLLER] model file upload n:"+file.getName()+" id:"+id);
      modelManager.addModelFile(file, id, user);
      return ResponseEntity.ok(response);
    } catch (Exception e) {
      response.setError(true);
      response.setMessage(e.getMessage());
      return ResponseEntity.badRequest().body(response);
    }
  }

  @GetMapping("/download")
  public ResponseEntity<?> downloadModelFile(@RequestParam(value = "id", required = true) Long id) {
    ResponseMessage response = new ResponseMessage(false, "");
  
    try {
      Model model = modelManager.getModel(id);
      return ResponseEntity.ok()
        .contentType(MediaType.parseMediaType(model.getFileType()))
        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + model.getName() + "\"")
        .body(new InputStreamResource(modelManager.getModelFile(id)));
    } catch (Exception e) {
      response.setError(true);
      response.setMessage(e.getMessage());
      return ResponseEntity.badRequest().body(response);
    }
  }
}