package mozen.web;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mozen.business.IUserManager;
import mozen.messages.ResponseMessage;
import mozen.messages.SignupMessage;
import mozen.messages.UserUpdateMessage;
import mozen.model.User;
import mozen.utils.JwtUtils;
import mozen.utils.UserHelper;

@RestController
@RequestMapping("/api/users")
@CrossOrigin
public class UserController {
  @Autowired
  IUserManager userManager;

  @GetMapping("")
  public User getUser() {
    String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    return userManager.getUserByUsername(username);
  }

  @GetMapping("/list")
  public Collection<User> getUserList() {
    return userManager.getUsers();
  }

  @PutMapping("")
  public ResponseEntity<ResponseMessage> changeUserDetails(@RequestBody @Valid UserUpdateMessage message) {
    ResponseMessage response = new ResponseMessage(false, "");
    User user = UserHelper.getCurrentUser(userManager);
    if(user == null) {
      response.setError(true);
      response.setMessage("User unknown");
      return ResponseEntity.badRequest().body(response);
    }

    try {
      userManager.updateUser(user ,message);
      return ResponseEntity.ok(response);
    } catch (Exception e) {
      response.setError(true);
      response.setMessage(e.getMessage());
      return ResponseEntity.badRequest().body(response);
    }
  }

  @DeleteMapping("")
  public ResponseEntity<ResponseMessage> deleteUser(@RequestParam Long id) {
    ResponseMessage response = new ResponseMessage(false, "");
    User user = UserHelper.getCurrentUser(userManager);
    if(user == null) {
      response.setError(true);
      response.setMessage("User unknown");
      return ResponseEntity.badRequest().body(response);
    }

    try {
      userManager.removeUser(user, id);
      return ResponseEntity.ok().build();
    } catch (Exception e) {
      response.setError(true);
      response.setMessage(e.getMessage());
      return ResponseEntity.badRequest().body(response);
    }
  }

  @PostMapping("/signup")
  public ResponseEntity<ResponseMessage> addUser(@RequestBody @Valid SignupMessage message, BindingResult result) {
    System.err.println("SIGNUP u:"+message.getUsername()+" e:"+message.getEmail()+" p:"+message.getPassword());
    ResponseMessage response = new ResponseMessage(false, "");

    try {
      User user = userManager.addUser(message);
      Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
      grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_"+user.getRole()));
      response.setMessage(JwtUtils.generateToken(user.getUsername(), grantedAuthorities));
      return ResponseEntity.ok(response);
    } catch (Exception e) {
      response.setError(true);
      response.setMessage(e.getMessage());
      return ResponseEntity.badRequest().body(response);
    }
  }

  @GetMapping("/setAdmin")
  public ResponseEntity<ResponseMessage> setAdmin(@RequestParam(value = "id", required = true) Long id) {
    ResponseMessage response = new ResponseMessage(false, "");
    User user = UserHelper.getCurrentUser(userManager);
    if(user == null) {
      response.setError(true);
      response.setMessage("User unknown");
      return ResponseEntity.badRequest().body(response);
    }

    try {
      userManager.setAdmin(id, user);
      return ResponseEntity.ok(response);
    } catch (Exception e) {
      response.setError(true);
      response.setMessage(e.getMessage());
      return ResponseEntity.badRequest().body(response);
    }
  }
}