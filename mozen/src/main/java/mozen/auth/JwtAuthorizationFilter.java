package mozen.auth;

import java.io.IOException;
import java.security.Key;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import mozen.utils.JwtUtils;

public class JwtAuthorizationFilter extends BasicAuthenticationFilter {

  public JwtAuthorizationFilter(AuthenticationManager authenticationManager) {
    super(authenticationManager);
  }

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) 
  throws IOException, ServletException {
    String header = request.getHeader("Authorization");

    if(header == null || !header.startsWith("Bearer ")) {
      chain.doFilter(request, response);
      return;
    }

    UsernamePasswordAuthenticationToken auth = getAuthentication(request);
    SecurityContextHolder.getContext().setAuthentication(auth);
    
    chain.doFilter(request, response);
  }

  private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
    String token = request.getHeader("Authorization");
    if(token != null) {
      try {
        Key key = JwtUtils.generateKey();
        Claims claims = Jwts.parser().setSigningKey(key).parseClaimsJws(token.replace("Bearer ", "")).getBody();
        String username = (String) claims.get("username");
        String role = (String) claims.get("role");
        if (username != null && role != null) {
          System.err.println("Auth user u:"+username+" r:"+role);
          Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
          grantedAuthorities.add(new SimpleGrantedAuthority(role));
          return new UsernamePasswordAuthenticationToken(username, null, grantedAuthorities);
        }
      } catch (Exception e) {
        return null;
      }

      return null;
    }
    return null;
  }

}