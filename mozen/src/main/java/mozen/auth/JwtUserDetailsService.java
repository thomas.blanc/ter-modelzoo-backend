package mozen.auth;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import mozen.business.IUserManager;

@Service
public class JwtUserDetailsService implements UserDetailsService {
  @Autowired
  IUserManager manager;

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    System.err.println("[USER DETAILS] u:"+username);
    mozen.model.User user = manager.getUserByUsername(username);
    if(user == null) throw new UsernameNotFoundException(username);
    
    /* 
    // Gestion multi roles
    // Plus propre (Transformer model.Role en Entity et model.User.role en liste de Role)
    for(Role role : user.getRoles()){
      grantedAuthorities.add(new SimpleGrantedAuthority(role.getName()));
    }
    */

    Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
    grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_"+user.getRole().toString()));    

    return new User(user.getUsername(), user.getPassword(), grantedAuthorities);
  }  
}