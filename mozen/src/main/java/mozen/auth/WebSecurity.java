package mozen.auth;

import java.util.Arrays;
import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.PathRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@EnableWebSecurity
public class WebSecurity extends WebSecurityConfigurerAdapter{
  @Autowired
  private JwtUserDetailsService userDetailsService;

  @Autowired
  private BCryptPasswordEncoder bCryptPasswordEncoder;

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http
      .httpBasic()
    .and()
      .csrf().disable()
      .cors()
    .and()
      .authorizeRequests() 
        // PUBLIC SECTION
        .requestMatchers(PathRequest.toStaticResources().atCommonLocations()).permitAll()
        .antMatchers("/").permitAll()
        .antMatchers(HttpMethod.POST, "/api/users/login").permitAll()
        .antMatchers(HttpMethod.POST, "/api/users/signup").permitAll()
        .antMatchers(HttpMethod.GET, "/api/search").permitAll()
        .antMatchers(HttpMethod.GET, "/api/comments").permitAll()
        .antMatchers(HttpMethod.GET, "/api/tags").permitAll()
        .antMatchers(HttpMethod.GET, "/api/models").permitAll()
        .antMatchers(HttpMethod.GET, "/api/models/download").permitAll()
        .antMatchers(HttpMethod.GET, "/api/layers/download").permitAll()

        // ADMIN SECTION
        .antMatchers(HttpMethod.GET, "/api/models/setVerified").hasRole("ADMIN")
        .antMatchers(HttpMethod.GET, "/api/users/setAdmin").hasRole("ADMIN")
        .antMatchers(HttpMethod.DELETE, "/api/tags").hasRole("ADMIN")
        .antMatchers(HttpMethod.DELETE, "/api/tags/category").hasRole("ADMIN")
        .antMatchers(HttpMethod.POST, "/api/tags/category").hasRole("ADMIN")
        .antMatchers(HttpMethod.GET, "/api/models/list").hasRole("ADMIN")
        .antMatchers(HttpMethod.GET, "/api/users/list").hasRole("ADMIN")
        .antMatchers(HttpMethod.GET, "/api/comments/list").hasRole("ADMIN")

        // USER SECTION
        .antMatchers("/api/**").authenticated()

        // DEFAULT
        .anyRequest().permitAll()
    .and()
      .addFilter(new JwtAuthenticationFilter(authenticationManager()))
      .addFilter(new JwtAuthorizationFilter(authenticationManager()))
      .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
  }

  @Override
  public void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
  }

  @Bean
  public CorsConfigurationSource corsConfigurationSource() {
      final CorsConfiguration configuration = new CorsConfiguration();
      configuration.setAllowedOrigins(Collections.unmodifiableList(Arrays.asList("*")));
      configuration.setAllowedMethods(Collections.unmodifiableList(Arrays.asList("HEAD", "GET", "POST", "PUT", "DELETE", "PATCH")));
      // setAllowCredentials(true) is important, otherwise:
      // The value of the 'Access-Control-Allow-Origin' header in the response must not be the wildcard '*' when the request's credentials mode is 'include'.
      configuration.setAllowCredentials(true);
      // setAllowedHeaders is important! Without it, OPTIONS preflight request
      // will fail with 403 Invalid CORS request
      configuration.setAllowedHeaders(Collections.unmodifiableList(Arrays.asList("Authorization", "Cache-Control", "Content-Type")));
      final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
      source.registerCorsConfiguration("/**", configuration);
      return source;
  }
}