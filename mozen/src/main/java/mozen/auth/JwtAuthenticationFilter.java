package mozen.auth;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import mozen.messages.LoginMessage;
import mozen.messages.ResponseMessage;
import mozen.utils.JwtUtils;

public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter{
  private AuthenticationManager authenticationManager;

  public JwtAuthenticationFilter(AuthenticationManager authenticationManager){
    this.authenticationManager = authenticationManager;

    setFilterProcessesUrl("/api/users/login");
  }

  @Override
  public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res) throws AuthenticationException {
    try {
      LoginMessage message = new ObjectMapper().readValue(req.getInputStream(), LoginMessage.class);

      System.err.println("Login user n:"+message.getUsername()+" p:"+message.getPassword());

      return authenticationManager.authenticate(
        new UsernamePasswordAuthenticationToken(message.getUsername(), message.getPassword(), new ArrayList<>())
      );
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  protected void successfulAuthentication(HttpServletRequest req, HttpServletResponse res, FilterChain chain, Authentication auth) 
  throws IOException, ServletException {
    User user = (User) auth.getPrincipal();
    String token = JwtUtils.generateToken(user.getUsername(), user.getAuthorities());
    res.getWriter().write(new ObjectMapper().writeValueAsString(new ResponseMessage(false, token)));
    // res.addHeader("Authorization", "Bearer " + token);
  }
}