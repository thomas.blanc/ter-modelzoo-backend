package mozen.business;

import java.util.Collection;

import mozen.messages.CommentMessage;
import mozen.model.Comment;
import mozen.model.User;

public interface ICommentManager {
  void addComment(CommentMessage message, User user) throws Exception;
  void removeComment(Long id, User user) throws Exception;

  Collection<Comment> getComments();
  Collection<Comment> getCommentFromModel(Long modelId) throws Exception;
  Collection<Comment> getCommentFromUser(Long userId) throws Exception;
}