package mozen.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import mozen.model.CustomLayer;
import mozen.model.Model;
import mozen.model.User;
import mozen.repos.CustomLayerRepository;
import mozen.repos.ModelRepository;
import mozen.utils.Md5Utils;
import mozen.utils.UserHelper;

@Service
public class LayerManager implements ILayerManager{
  @Autowired
  private ModelRepository modelRepo;

  @Autowired
  private CustomLayerRepository layerRepo;

  @Override
  public CustomLayer getLayer(long id) {
    return layerRepo.findById(id).get();
  }

  @Override
  public void addLayerFile(MultipartFile file, long modelId, String name, User user) throws Exception {
    Model model = modelRepo.findById(modelId).get();

    if (model == null)
      throw new Exception("Unknown model");
    if (!UserHelper.isAuthor(model.getAuthor(), user))
      throw new Exception("Not the author");
    
    CustomLayer layer = new CustomLayer();
    layer.setName(name);
    layer.setChecksum(Md5Utils.getMD5(file));
    layer.setFile(file.getBytes());
    layer.setFileName(file.getOriginalFilename());
    layer.setFileType(file.getContentType());
    layer.setModel(model);

    model.getCustomLayers().add(layer);

    modelRepo.save(model);
  }

  @Override
  public void removeLayer(long id, User user) throws Exception {
    CustomLayer layer = layerRepo.findById(id).get();

    if (layer == null)
      throw new Exception("Unknown layer");
    if (!UserHelper.isAuthor(layer.getModel().getAuthor(), user))
      throw new Exception("Not the author");

    layerRepo.deleteById(id);
  }
}