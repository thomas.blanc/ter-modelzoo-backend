package mozen.business;

import mozen.model.User;

public interface IVoteManager {
  void addUpvote(Long id, User user) throws Exception;
  void addDownvote(Long id, User user) throws Exception;

  void removeUpvote(Long id, User user) throws Exception;
  void removeDownvote(Long id, User user) throws Exception;

  String getStatus(Long id, User user) throws Exception;
}