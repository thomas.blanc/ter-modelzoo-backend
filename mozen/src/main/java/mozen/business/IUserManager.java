package mozen.business;

import java.util.Collection;

import mozen.messages.SignupMessage;
import mozen.messages.UserUpdateMessage;
import mozen.model.User;

public interface IUserManager {
  User addUser(SignupMessage message);
  void updateUser(User user, UserUpdateMessage message) throws Exception;
  void removeUser(User user, Long id);

  Collection<User> getUsers();
  User getUser(Long id);
  User getUserByUsername(String username);

  void setAdmin(Long id, User user) throws Exception;
}