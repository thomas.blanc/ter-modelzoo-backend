package mozen.business;

import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import mozen.model.Role;
import mozen.messages.SignupMessage;
import mozen.messages.UserUpdateMessage;
import mozen.model.User;
import mozen.repos.UserRepository;
import mozen.utils.UserHelper;

@Service
public class UserManager implements IUserManager {

  @Autowired
  UserRepository repo;

  @Autowired
  BCryptPasswordEncoder bCryptPasswordEncoder;

  @Override
  public User addUser(SignupMessage message) {
    System.err.println("[USER MANAGER] add new user u:" + message.getUsername() + " e:" + message.getEmail() + " p:"
        + message.getPassword());
    User u = new User();
    u.setEmail(message.getEmail());
    u.setUsername(message.getUsername());
    u.setPassword(bCryptPasswordEncoder.encode(message.getPassword()));
    u.setRole(Role.DEFAULT);

    repo.save(u);
    return u;
  }

  @Override
  public void updateUser(User user, UserUpdateMessage message) throws Exception {
    User userToUpdate = getUser(message.getId());

    if(!isRightUser(user, userToUpdate)) 
      throw new Exception("Not the user");

    userToUpdate.setEmail(message.getEmail());
    userToUpdate.setUsername(message.getUsername());
    if (message.getPassword() != null)
      userToUpdate.setPassword(bCryptPasswordEncoder.encode(message.getPassword()));

    repo.save(userToUpdate);
  }

  @Override
  public void removeUser(User user, Long id) {
    User userToRemove = repo.findById(id).get();
    if(isRightUser(user, userToRemove)) repo.deleteById(id);
  }

  @Override
  public User getUser(Long id) {
    Optional<User> user = repo.findById(id);
    if(user.isPresent()) return user.get();
    else return null;
  }

  @Override
  public User getUserByUsername(String username) {
    return repo.findByUsername(username);
  }

  @Override
  public Collection<User> getUsers() {
    return repo.findAll();
  }

  private boolean isRightUser(User userToCheck, User user) {
    if (userToCheck.getRole() == Role.ADMIN) return true;
    return userToCheck.getId().equals(user.getId());
  }

  @Override
  public void setAdmin(Long id, User user) throws Exception {
    User userToUpdate = getUser(id);
    if (userToUpdate == null)
      throw new Exception("Unknown user");
    if (!UserHelper.isAdmin(user))
      throw new Exception("Not admin");

    Role role = userToUpdate.getRole();
    if (role == Role.ADMIN) userToUpdate.setRole(Role.DEFAULT);
    else userToUpdate.setRole(Role.ADMIN);

    repo.save(userToUpdate);
  }
  
}