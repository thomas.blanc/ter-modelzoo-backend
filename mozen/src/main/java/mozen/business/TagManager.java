package mozen.business;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mozen.messages.TagCategoryMessage;
import mozen.messages.TagMessage;
import mozen.model.Tag;
import mozen.model.TagCategory;
import mozen.repos.TagCategoryRepository;
import mozen.repos.TagRepository;

@Service
public class TagManager implements ITagManager {
  @Autowired
  private TagRepository tagRepo;

  @Autowired
  private TagCategoryRepository tagCategoryRepo;
  
  @Override
  public void addTag(TagMessage message) {
    System.err.println("[MODEL MANAGER] add tag n:" + message.getName() + " c:" + message.getCategoryId());
    Long categoryId = message.getCategoryId();
    TagCategory category = tagCategoryRepo.findById(categoryId).get();

    Tag tag = new Tag();
    tag.setName(message.getName());
    tag.setCategory(category);

    tagRepo.save(tag);
  }

  @Override
  public void removeTag(long id) {
    tagRepo.deleteById(id);
  }

  @Override
  public void addCategory(TagCategoryMessage message) {
    TagCategory category = new TagCategory();
    category.setName(message.getName());
    tagCategoryRepo.save(category);
  }

  @Override
  public void removeCategory(long id) {
    tagCategoryRepo.deleteById(id);
  }

  @Override
  public Collection<TagCategory> getTags() {
    return tagCategoryRepo.findAll();
  }
}