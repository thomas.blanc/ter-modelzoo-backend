package mozen.business;

import java.util.Collection;

import mozen.messages.TagCategoryMessage;
import mozen.messages.TagMessage;
import mozen.model.TagCategory;

public interface ITagManager {
  void addTag(TagMessage message);
  void removeTag(long id);

  void addCategory(TagCategoryMessage message);
  void removeCategory(long id);

  Collection<TagCategory> getTags();
}