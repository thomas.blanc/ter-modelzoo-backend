package mozen.business;

import java.io.InputStream;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import mozen.messages.ModelMessage;
import mozen.model.Model;
import mozen.messages.SearchResult;
import mozen.model.Tag;
import mozen.model.User;
import mozen.repos.ModelRepository;
import mozen.repos.ModelStore;
import mozen.repos.TagRepository;
import mozen.utils.Md5Utils;
import mozen.utils.UserHelper;

@Service
public class ModelManager implements IModelManager {

  @Autowired
  private ModelRepository modelRepo;

  @Autowired
  private TagRepository tagRepo;

  @Autowired
  private ModelStore modelStore;

  @Override
  public Model addModel(ModelMessage message, User user) throws Exception {
    Model model = new Model();

    System.err.println("[MODEL MANAGER] add model n:" + message.getName() + " sd:" + message.getShortDescription()
        + " ld:" + message.getLongDescription() + " t:" + message.getTags() + " l:" + message.getCustomLayers());

    model.setName(message.getName());
    model.setShortDescription(message.getShortDescription());
    model.setLongDescription(message.getLongDescription());
    model.setPerformance(message.getPerformance());
    model.setPerformanceUnit(message.getPerformanceUnit());
    model.setParameterCount(message.getParameterCount());

    // Standardise la mesure de performance.
    // C'est bancale mais difficile de trier autrement en mélangeant les unitées de
    // performances
    if (message.getPerformanceLowerIsBetter()) {
      model.setPerformanceIndex(message.getPerformance());
    } else {
      model.setPerformanceIndex(100 - message.getPerformance());
    }

    if (message.getTags() != null) {
      Set<Tag> tags = new HashSet<Tag>();
      for (String tagName : message.getTags()) {
        Tag tag = tagRepo.findByName(tagName);
        tags.add(tag);
      }
      model.setTags(tags);
    }

    model.setIsVerified(false);
    model.setVotes(0);
    model.setAdded(new Date());
    model.setLastModified(new Date());
    model.setAuthor(user);

    modelRepo.save(model);

    return model;
  }

  @Override
  public void addModelFile(MultipartFile file, long id, User user) throws Exception {
    Model model = modelRepo.findById(id).get();

    if (model == null)
      throw new Exception("Unknown model");
    if (!UserHelper.isAuthor(model.getAuthor(), user))
      throw new Exception("Not the author");

    model.setChecksum(Md5Utils.getMD5(file));
    modelStore.setContent(model, file.getInputStream());
    model.setFileName(file.getOriginalFilename());
    model.setFileType(file.getContentType());

    modelRepo.save(model);
  }

  @Override
  public void updateModel(ModelMessage message, long id, User user) throws Exception {
    Model model = modelRepo.findById(id).get();

    if (model == null)
      throw new Exception("Unknown model");
    if (!UserHelper.isAuthor(model.getAuthor(), user))
      throw new Exception("Not the author");

    if (message.getName() != null)
      model.setName(message.getName());
    if (message.getShortDescription() != null)
      model.setShortDescription(message.getShortDescription());
    if (message.getLongDescription() != null)
      model.setLongDescription(message.getLongDescription());

    model.setPerformance(message.getPerformance());
    if (message.getPerformanceUnit() != null)
      model.setPerformanceUnit(message.getPerformanceUnit());
    model.setParameterCount(message.getParameterCount());

    if (message.getTags() != null) {
      Set<Tag> tags = new HashSet<Tag>();
      for (String tagName : message.getTags()) {
        Tag tag = tagRepo.findByName(tagName);
        tags.add(tag);
      }
      model.setTags(tags);
    }

    model.setLastModified(new Date());

    modelRepo.save(model);
  }

  @Override
  public void removeModel(long id, User user) throws Exception {
    Model model = modelRepo.findById(id).get();

    if (model == null)
      throw new Exception("Unknown model");
    if (!UserHelper.isAuthor(model.getAuthor(), user))
      throw new Exception("Not the author");

    System.err.println("[MODEL MANAGER] Delete 1");

    System.err.println("[MODEL MANAGER] Delete 2");

    modelRepo.delete(model);

    System.err.println("[MODEL MANAGER] Delete 3");
  }

  @Override
  public void setVerified(long id, User user) throws Exception {
    Model model = modelRepo.findById(id).get();

    if (model == null)
      throw new Exception("Unknown model");
    if (!UserHelper.isAuthor(model.getAuthor(), user))
      throw new Exception("Not admin");

    model.setIsVerified(!model.getIsVerified());
    modelRepo.save(model);
  }

  @Override
  public SearchResult findModel(String name, int page, int size, String sort, Collection<String> tagsName) {
    Pageable paging;

    // Tri dans le bon sens selon l'attribut
    switch (sort) {
      case "votes":
        paging = PageRequest.of(page - 1, size, Sort.by(sort).descending());
        break;
      case "lastModified":
        paging = PageRequest.of(page - 1, size, Sort.by(sort).descending());
        break;
      case "performance":
        paging = PageRequest.of(page - 1, size, Sort.by("performanceIndex"));
        break;
      case "parameterCount":
        paging = PageRequest.of(page - 1, size, Sort.by(sort));
        break;
      default:
        paging = PageRequest.of(page - 1, size, Sort.by("name"));
    }

    Page<Model> resultPage;
    if (tagsName == null) {
      resultPage = modelRepo.findByNameContainingIgnoreCase(name, paging);
    } else {
      Collection<Tag> tags = tagRepo.findByNameIn(tagsName);
      resultPage = modelRepo.findDistinctByNameContainingIgnoreCaseAndTagsIn(name, tags, paging);
    }

    SearchResult result = new SearchResult();
    if (resultPage.hasContent()) {
      result.setTotalResult((int) resultPage.getTotalElements());
      result.setTotalPage((int) resultPage.getTotalPages());
      result.setPage(resultPage.getNumber() + 1);
      result.setModels(resultPage.getContent());
    } else {
      result.setPage(0);
      result.setTotalResult(0);
      result.setTotalPage(0);
    }
    return result;
  }

  @Override
  public Model getModel(long id) {
    Optional<Model> model = modelRepo.findById(id);
    if (model.isPresent())
      return model.get();
    else
      return null;
  }

  @Override
  public Collection<Model> getModels() {
    return modelRepo.findAll();
  }

  @Override
  public InputStream getModelFile(long id) throws Exception {
    Model model = getModel(id);

    if (model == null)
      throw new Exception("Unknown model");

    return modelStore.getContent(model);
  }
}