package mozen.business;

import org.springframework.web.multipart.MultipartFile;

import mozen.model.CustomLayer;
import mozen.model.User;

public interface ILayerManager {
  void addLayerFile(MultipartFile file, long modelId, String name, User user) throws Exception;
  void removeLayer(long id, User user) throws Exception;
  
  CustomLayer getLayer(long id);
}