package mozen.business;

import java.io.InputStream;
import java.util.Collection;

import org.springframework.web.multipart.MultipartFile;

import mozen.messages.ModelMessage;
import mozen.model.Model;
import mozen.messages.SearchResult;
import mozen.model.User;

public interface IModelManager {
  Model addModel(ModelMessage message, User user) throws Exception;
  void addModelFile(MultipartFile file, long id, User user) throws Exception;
  void updateModel(ModelMessage message, long id, User user) throws Exception;
  void removeModel(long id, User user) throws Exception; 

  void setVerified(long id, User user) throws Exception;

  Model getModel(long id);
  Collection<Model> getModels();

  InputStream getModelFile(long id) throws Exception;

  SearchResult findModel(String name, int page, int size, String sort, Collection<String> tagsName);
}