package mozen.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mozen.model.Model;
import mozen.model.User;
import mozen.repos.ModelRepository;

@Service
public class VoteManager implements IVoteManager {
  
  @Autowired
  private ModelRepository modelRepo;

  @Override
  public void addUpvote(Long id, User user) throws Exception {
    Model model = modelRepo.findById(id).get();

    if (model == null)
      throw new Exception("Unknown model");
    if (user.getUpvotes().contains(model))
      throw new Exception("Up present");

    model.setVotes(model.getVotes()+1);
    model.getUsersUpvotes().add(user);

    modelRepo.save(model);
  }

  @Override
  public void addDownvote(Long id, User user) throws Exception {
    Model model = modelRepo.findById(id).get();

    if (model == null)
      throw new Exception("Unknown model");
    if (user.getDownvotes().contains(model))
      throw new Exception("Downvote present");

    model.setVotes(model.getVotes()-1);
    model.getUsersDownvotes().add(user);

    modelRepo.save(model);
  }

  @Override
  public void removeUpvote(Long id, User user) throws Exception {
    Model model = modelRepo.findById(id).get();

    if (model == null)
      throw new Exception("Unknown model");
    if (!user.getUpvotes().contains(model))
      throw new Exception("No upvote");

    model.setVotes(model.getVotes()-1);
    model.getUsersUpvotes().remove(user);

    modelRepo.save(model);
  }

  @Override
  public void removeDownvote(Long id, User user) throws Exception {
    Model model = modelRepo.findById(id).get();

    if (model == null)
      throw new Exception("Unknown model");
    if (!user.getDownvotes().contains(model))
      throw new Exception("No downvote");

    model.setVotes(model.getVotes()+1);
    model.getUsersDownvotes().remove(user);

    modelRepo.save(model);
  }

  @Override
  public String getStatus(Long id, User user) throws Exception {
    Model model = modelRepo.findById(id).get();

    if (model == null)
      throw new Exception("Unknown model");

    if (user.getUpvotes().contains(model)) return "up";
    if (user.getDownvotes().contains(model)) return "down";
    return "none";
  }
  
}