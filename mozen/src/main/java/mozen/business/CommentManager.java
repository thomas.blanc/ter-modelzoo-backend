package mozen.business;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mozen.messages.CommentMessage;
import mozen.model.Comment;
import mozen.model.Model;
import mozen.model.User;
import mozen.repos.CommentRepository;
import mozen.repos.ModelRepository;
import mozen.repos.UserRepository;
import mozen.utils.UserHelper;

@Service
public class CommentManager implements ICommentManager {
  @Autowired
  private ModelRepository modelRepo;

  @Autowired
  private UserRepository userRepo;

  @Autowired
  private CommentRepository commentRepo;

  @Override
  public void addComment(CommentMessage message, User user) throws Exception {
    Model model = modelRepo.findById(message.getModelId()).get();

    if (model == null)
      throw new Exception("Unknown model");
      
    Comment comment = new Comment();
    comment.setAuthor(user);
    comment.setContent(message.getContent());
    comment.setModel(model);
    comment.setAdded(new Date());

    commentRepo.save(comment);
  }

  @Override
  public void removeComment(Long id, User user) throws Exception {
    Comment comment = commentRepo.findById(id).get();

    if (comment == null)
      throw new Exception("Unknown comment");
    if (!UserHelper.isAuthor(comment.getAuthor(), user))
      throw new Exception("Not the author");
    
    commentRepo.delete(comment);
  }

  @Override
  public Collection<Comment> getCommentFromModel(Long modelId) throws Exception {
    Model model = modelRepo.findById(modelId).get();

    if (model == null)
      throw new Exception("Unknown model");

    return commentRepo.findByModel(model);
  }

  @Override
  public Collection<Comment> getCommentFromUser(Long userId) throws Exception {
    User user = userRepo.findById(userId).get();

    if (user == null)
      throw new Exception("Unknown user");

    return commentRepo.findByAuthor(user);
  }

  @Override
  public Collection<Comment> getComments() {
    return commentRepo.findAll();
  }
  
}