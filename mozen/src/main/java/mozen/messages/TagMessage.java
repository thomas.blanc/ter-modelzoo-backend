package mozen.messages;

import java.io.Serializable;

public class TagMessage implements Serializable {
  private static final long serialVersionUID = 1L;

  private long categoryId;
  
  private String name;

  public TagMessage() {
  }

  public TagMessage(long categoryId, String name) {
    this.categoryId = categoryId;
    this.name = name;
  }

  public long getCategoryId() {
    return this.categoryId;
  }

  public void setCategoryId(long categoryId) {
    this.categoryId = categoryId;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

}