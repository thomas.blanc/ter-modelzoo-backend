package mozen.messages;

import java.io.Serializable;

public class TagCategoryMessage implements Serializable{
  private static final long serialVersionUID = 1L;
  
  private String name;

  public TagCategoryMessage() {
  }

  public TagCategoryMessage(String name) {
    this.name = name;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }
}