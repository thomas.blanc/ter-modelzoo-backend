package mozen.messages;

import java.io.Serializable;

public class CommentMessage implements Serializable {
  private static final long serialVersionUID = 1L;
  
  private String content;

  private Long modelId;

  public CommentMessage() {
  }

  public CommentMessage(String content, Long modelId) {
    this.content = content;
    this.modelId = modelId;
  }

  public String getContent() {
    return this.content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public Long getModelId() {
    return this.modelId;
  }

  public void setModelId(Long modelId) {
    this.modelId = modelId;
  }
  
}