package mozen.messages;

import java.io.Serializable;

public class VoteStatusMessage implements Serializable{
  private static final long serialVersionUID = 1L;

  private String status;

  public VoteStatusMessage() {
  }

  public VoteStatusMessage(String status) {
    this.status = status;
  }

  public String getStatus() {
    return this.status;
  }

  public void setStatus(String status) {
    this.status = status;
  }
}
  