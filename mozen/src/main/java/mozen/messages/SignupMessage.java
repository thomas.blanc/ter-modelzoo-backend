package mozen.messages;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class SignupMessage implements Serializable {
  private static final long serialVersionUID = 1L;
  
  @NotNull
  @Size(min = 1, max = 30)
  private String username;

  @NotNull
  @Size(min = 1, max = 60)
  //@Pattern(regexp="(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*-_=+])[a-zA-Z0-9!@#$%^&*-_=+](?=\\S+$).{8,35}")
  private String password;
  
  @NotNull
  @Size(min = 1, max = 30)
  @Pattern(regexp="([a-z0-9])+([.]([a-z0-9])+)?@([a-z])+(([.]([a-z])+)?|([-]([a-z])+)?)+.([a-z]){2,}")
  private String email;


  public SignupMessage() {
  }


  public SignupMessage(String username, String password, String email) {
    this.username = username;
    this.password = password;
    this.email = email;
  }


  public String getUsername() {
    return this.username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return this.password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getEmail() {
    return this.email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

}