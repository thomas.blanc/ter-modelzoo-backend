package mozen.messages;

import java.io.Serializable;

public class ResponseMessage implements Serializable {
  private static final long serialVersionUID = 1L;

  private boolean error;

  private String message;

  public ResponseMessage() {
  }

  public ResponseMessage(boolean error, String message) {
    this.error = error;
    this.message = message;
  }

  public boolean isError() {
    return this.error;
  }

  public boolean getError() {
    return this.error;
  }

  public void setError(boolean error) {
    this.error = error;
  }

  public String getMessage() {
    return this.message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

}