package mozen.messages;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class UserUpdateMessage implements Serializable{
  private static final long serialVersionUID = 1L;

  @NotNull
  private Long id;
  
  @NotNull
  @Size(min = 1, max = 30)
  private String username;

  @Size(min = 1, max = 60)
  private String password;
  
  @NotNull
  @Size(min = 1, max = 30)
  @Pattern(regexp="([a-z0-9])+([.]([a-z0-9])+)?@([a-z])+(([.]([a-z])+)?|([-]([a-z])+)?)+.([a-z]){2,}")
  private String email;

  public UserUpdateMessage() {
  }

  public UserUpdateMessage(Long id, String username, String password, String email) {
    this.id = id;
    this.username = username;
    this.password = password;
    this.email = email;
  }

  public Long getId() {
    return this.id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUsername() {
    return this.username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return this.password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getEmail() {
    return this.email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

}