package mozen.messages;

import java.io.Serializable;
import java.util.Collection;

import mozen.model.Model;

public class SearchResult implements Serializable {
  private static final long serialVersionUID = 1L;

  private int page;
  private int totalResult;
  private int totalPage;
  private Collection<Model> models;

  public SearchResult() {
  }

  public SearchResult(int page, int totalResult, int totalPage, Collection<Model> models) {
    this.page = page;
    this.totalResult = totalResult;
    this.totalPage = totalPage;
    this.models = models;
  }

  public int getPage() {
    return this.page;
  }

  public void setPage(int page) {
    this.page = page;
  }

  public int getTotalResult() {
    return this.totalResult;
  }

  public void setTotalResult(int totalResult) {
    this.totalResult = totalResult;
  }

  public int getTotalPage() {
    return this.totalPage;
  }

  public void setTotalPage(int totalPage) {
    this.totalPage = totalPage;
  }

  public Collection<Model> getModels() {
    return this.models;
  }

  public void setModels(Collection<Model> models) {
    this.models = models;
  }
  
}