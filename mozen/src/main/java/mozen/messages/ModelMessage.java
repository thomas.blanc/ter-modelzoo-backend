package mozen.messages;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ModelMessage implements Serializable {
  private static final long serialVersionUID = 1L;
  
  @NotNull
  @Size(min = 3, max = 100)
  private String name;

  @Size(min = 0, max = 200)
  private String shortDescription;

  @Size(min = 0, max = 5000)
  private String longDescription;

  private String [] tags;

  private String [] customLayers;

  private double performance;

  private String performanceUnit;

  private boolean performanceLowerIsBetter;

  private int parameterCount;

  public ModelMessage() {
  }

  public ModelMessage(String name, String shortDescription, String longDescription, String[] tags, String[] customLayers, double performance, String performanceUnit, boolean performanceLowerIsBetter, int parameterCount) {
    this.name = name;
    this.shortDescription = shortDescription;
    this.longDescription = longDescription;
    this.tags = tags;
    this.customLayers = customLayers;
    this.performance = performance;
    this.performanceUnit = performanceUnit;
    this.performanceLowerIsBetter = performanceLowerIsBetter;
    this.parameterCount = parameterCount;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getShortDescription() {
    return this.shortDescription;
  }

  public void setShortDescription(String shortDescription) {
    this.shortDescription = shortDescription;
  }

  public String getLongDescription() {
    return this.longDescription;
  }

  public void setLongDescription(String longDescription) {
    this.longDescription = longDescription;
  }

  public String[] getTags() {
    return this.tags;
  }

  public void setTags(String[] tags) {
    this.tags = tags;
  }

  public String[] getCustomLayers() {
    return this.customLayers;
  }

  public void setCustomLayers(String[] customLayers) {
    this.customLayers = customLayers;
  }

  public double getPerformance() {
    return this.performance;
  }

  public void setPerformance(double performance) {
    this.performance = performance;
  }

  public String getPerformanceUnit() {
    return this.performanceUnit;
  }

  public void setPerformanceUnit(String performanceUnit) {
    this.performanceUnit = performanceUnit;
  }

  public boolean isPerformanceLowerIsBetter() {
    return this.performanceLowerIsBetter;
  }

  public boolean getPerformanceLowerIsBetter() {
    return this.performanceLowerIsBetter;
  }

  public void setPerformanceLowerIsBetter(boolean performanceLowerIsBetter) {
    this.performanceLowerIsBetter = performanceLowerIsBetter;
  }

  public int getParameterCount() {
    return this.parameterCount;
  }

  public void setParameterCount(int parameterCount) {
    this.parameterCount = parameterCount;
  }


}