package mozen.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.springframework.content.commons.annotations.ContentId;
import org.springframework.content.commons.annotations.ContentLength;
import org.springframework.content.commons.annotations.MimeType;

@Entity
@Table
public class Model implements Serializable{
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  
  @Basic
  @Column(nullable = false, unique = true)
  @NotNull
  @Size(min = 3, max = 100)
  private String name;

  @Basic
  @Column
  @Size(min = 0, max = 200)
  private String shortDescription;

  @Basic
  @Column
  @Size(min = 0, max = 5000)
  private String longDescription;

  @Basic
	@Temporal(TemporalType.DATE)
	@Column
  private Date added;
  
  @Basic
	@Temporal(TemporalType.DATE)
	@Column
	private Date lastModified;

  @Basic
	@Column
  private int votes;

  @Basic
	@Column
  private double performance;

  @Basic
	@Column
  private String performanceUnit;

  @Basic
	@Column
  private double performanceIndex;

  @Basic
	@Column
  private int parameterCount;

  @Basic
	@Column
  private String fileName;

  @ContentId
  private String contentId;

  @ContentLength
  private long contentLength;

  @MimeType
  private String fileType;

  @Basic
	@Column
  private String checksum;

  @Basic
  @Column
  private boolean isVerified;

  @Basic
  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
  private User author;

  @Basic
  @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
  @JoinTable(name = "model_tag")
  private Set<Tag> tags;

  @Basic
  @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
  @JoinTable(name = "model_customLayers")
  private Set<CustomLayer> customLayers;

  @Basic
  @OneToMany(mappedBy = "model", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
  private Set<Comment> comments;

  @Basic
  @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
  @JoinTable(name = "model_upvotes")
  private Set<User> usersUpvotes;

  @Basic
  @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
  @JoinTable(name = "model_downvotes")
  private Set<User> usersDownvotes;

  public Model() {
  }

  public Model(Long id, String name, String shortDescription, String longDescription, Date added, Date lastModified, int votes, double performance, String performanceUnit, double performanceIndex, int parameterCount, String fileName, String contentId, long contentLength, String fileType, String checksum, boolean isVerified, User author, Set<Tag> tags, Set<CustomLayer> customLayers, Set<Comment> comments, Set<User> usersUpvotes, Set<User> usersDownvotes) {
    this.id = id;
    this.name = name;
    this.shortDescription = shortDescription;
    this.longDescription = longDescription;
    this.added = added;
    this.lastModified = lastModified;
    this.votes = votes;
    this.performance = performance;
    this.performanceUnit = performanceUnit;
    this.performanceIndex = performanceIndex;
    this.parameterCount = parameterCount;
    this.fileName = fileName;
    this.contentId = contentId;
    this.contentLength = contentLength;
    this.fileType = fileType;
    this.checksum = checksum;
    this.isVerified = isVerified;
    this.author = author;
    this.tags = tags;
    this.customLayers = customLayers;
    this.comments = comments;
    this.usersUpvotes = usersUpvotes;
    this.usersDownvotes = usersDownvotes;
  }
  
  @JsonIgnore
  public double getPerformanceIndex() {
    return this.performanceIndex;
  }

  public void setPerformanceIndex(double performanceIndex) {
    this.performanceIndex = performanceIndex;
  }  

  public Long getId() {
    return this.id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getShortDescription() {
    return this.shortDescription;
  }

  public void setShortDescription(String shortDescription) {
    this.shortDescription = shortDescription;
  }

  public String getLongDescription() {
    return this.longDescription;
  }

  public void setLongDescription(String longDescription) {
    this.longDescription = longDescription;
  }

  public Date getAdded() {
    return this.added;
  }

  public void setAdded(Date added) {
    this.added = added;
  }

  public Date getLastModified() {
    return this.lastModified;
  }

  public void setLastModified(Date lastModified) {
    this.lastModified = lastModified;
  }

  public int getVotes() {
    return this.votes;
  }

  public void setVotes(int votes) {
    this.votes = votes;
  }

  @JsonIgnore
  public String getContentId() {
    return this.contentId;
  }

  public void setContentId(String contentId) {
    this.contentId = contentId;
  }

  @JsonIgnore
  public long getContentLength() {
    return this.contentLength;
  }

  public void setContentLength(long contentLength) {
    this.contentLength = contentLength;
  }

  public User getAuthor() {
    return this.author;
  }

  public void setAuthor(User author) {
    this.author = author;
  }

  public Set<Tag> getTags() {
    return this.tags;
  }

  public void setTags(Set<Tag> tags) {
    this.tags = tags;
  }

  public Set<CustomLayer> getCustomLayers() {
    return this.customLayers;
  }

  public void setCustomLayers(Set<CustomLayer> customLayers) {
    this.customLayers = customLayers;
  }

  public String getFileType() {
    return this.fileType;
  }

  public void setFileType(String fileType) {
    this.fileType = fileType;
  }

  public double getPerformance() {
    return this.performance;
  }

  public void setPerformance(double performance) {
    this.performance = performance;
  }

  public String getPerformanceUnit() {
    return this.performanceUnit;
  }

  public void setPerformanceUnit(String performanceUnit) {
    this.performanceUnit = performanceUnit;
  }

  public int getParameterCount() {
    return this.parameterCount;
  }

  public void setParameterCount(int parameterCount) {
    this.parameterCount = parameterCount;
  }
  
  public String getChecksum() {
    return this.checksum;
  }

  public void setChecksum(String checksum) {
    this.checksum = checksum;
  }

  @JsonIgnore
  public Set<Comment> getComments() {
    return this.comments;
  }

  public void setComments(Set<Comment> comments) {
    this.comments = comments;
  }

  public boolean isIsVerified() {
    return this.isVerified;
  }

  public boolean getIsVerified() {
    return this.isVerified;
  }

  public void setIsVerified(boolean isVerified) {
    this.isVerified = isVerified;
  }

  public String getFileName() {
    return this.fileName;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  @JsonIgnore
  public Set<User> getUsersUpvotes() {
    return this.usersUpvotes;
  }

  public void setUsersUpvotes(Set<User> usersUpvotes) {
    this.usersUpvotes = usersUpvotes;
  }

  @JsonIgnore
  public Set<User> getUsersDownvotes() {
    return this.usersDownvotes;
  }

  public void setUsersDownvotes(Set<User> usersDownvotes) {
    this.usersDownvotes = usersDownvotes;
  }

}