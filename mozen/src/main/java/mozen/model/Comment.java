package mozen.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table
public class Comment implements Serializable{
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Basic
  @Column
  @Size(min = 0, max = 1000)
  private String content;

  @Basic
	@Temporal(TemporalType.DATE)
	@Column
  private Date added;

  @Basic
  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
  private Model model;
  
  @Basic
  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
  private User author;

  public Comment() {
  }

  public Comment(Long id, String content, Date added, Model model, User author) {
    this.id = id;
    this.content = content;
    this.added = added;
    this.model = model;
    this.author = author;
  }

  public Long getId() {
    return this.id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getContent() {
    return this.content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  @JsonIgnore
  public Model getModel() {
    return this.model;
  }

  public void setModel(Model model) {
    this.model = model;
  }

  public User getAuthor() {
    return this.author;
  }

  public void setAuthor(User author) {
    this.author = author;
  }

  public Date getAdded() {
    return this.added;
  }

  public void setAdded(Date added) {
    this.added = added;
  }

}