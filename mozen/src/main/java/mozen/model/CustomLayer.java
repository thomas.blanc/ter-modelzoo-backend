package mozen.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table
public class CustomLayer implements Serializable{
  private static final long serialVersionUID = 1L;
 
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  
  @Basic
	@Column(nullable = false)
  private String name;
  
  @Lob
	@Column(columnDefinition="BLOB")
  private byte[] file;

  @Basic
	@Column
  private String fileName;

  @Basic
	@Column
  private String fileType;

  @Basic
	@Column
  private String checksum;

  @Basic
  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  private Model model;

  public CustomLayer() {
  }

  public CustomLayer(Long id, String name, byte[] file, String fileName, String fileType, String checksum, Model model) {
    this.id = id;
    this.name = name;
    this.file = file;
    this.fileName = fileName;
    this.fileType = fileType;
    this.checksum = checksum;
    this.model = model;
  }
  
  public Long getId() {
    return this.id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @JsonIgnore
  public byte[] getFile() {
    return this.file;
  }

  public void setFile(byte[] file) {
    this.file = file;
  }

  @JsonIgnore
  public String getFileType() {
    return this.fileType;
  }

  public void setFileType(String fileType) {
    this.fileType = fileType;
  }

  @JsonIgnore
  public Model getModel() {
    return this.model;
  }

  public void setModel(Model model) {
    this.model = model;
  }

  public String getChecksum() {
    return this.checksum;
  }

  public void setChecksum(String checksum) {
    this.checksum = checksum;
  }

  public String getFileName() {
    return this.fileName;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }


}