package mozen.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table
public class User implements Serializable{
  private static final long serialVersionUID = 1L;
 
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  
  @Basic
  @Column(nullable = false, unique = true)
  @NotNull
  @Size(min = 1, max = 30)
  private String username;

  @Basic
  @Column(nullable = false)
  @NotNull
  @Size(min = 1, max = 60)
  //@Pattern(regexp="(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*-_=+])[a-zA-Z0-9!@#$%^&*-_=+](?=\\S+$).{8,35}")
  private String password;
  
  @Basic
  @Column(nullable = false, unique = true)
  @NotNull
  @Size(min = 1, max = 30)
  @Pattern(regexp="([a-z0-9])+([.]([a-z0-9])+)?@([a-z])+(([.]([a-z])+)?|([-]([a-z])+)?)+.([a-z]){2,}")
  private String email;
  
  @Basic
  @Column(nullable = false)
  @NotNull
  @Enumerated(EnumType.STRING)
  private Role role;
  
  @Basic
  @OneToMany(mappedBy = "author", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
  @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
  private Set<Model> models;

  @Basic
  @OneToMany(mappedBy = "author", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
  @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
  private Set<Comment> comments;

  @Basic
  @ManyToMany(fetch = FetchType.LAZY, mappedBy = "usersUpvotes")
  @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
  private Set<Model> upvotes;

  @Basic
  @ManyToMany(fetch = FetchType.LAZY, mappedBy = "usersDownvotes")
  @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
  private Set<Model> downvotes;


  public User() {
  }

  public User(Long id, String username, String password, String email, Role role, Set<Model> models, Set<Comment> comments, Set<Model> upvotes, Set<Model> downvotes) {
    this.id = id;
    this.username = username;
    this.password = password;
    this.email = email;
    this.role = role;
    this.models = models;
    this.comments = comments;
    this.upvotes = upvotes;
    this.downvotes = downvotes;
  }
  
  public Long getId() {
    return this.id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUsername() {
    return this.username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  @JsonIgnore
  public String getPassword() {
    return this.password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getEmail() {
    return this.email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  @JsonIgnore
  public Set<Model> getModels() {
    return this.models;
  }

  public void setModels(Set<Model> models) {
    this.models = models;
  }

  public Role getRole() {
    return this.role;
  }

  public void setRole(Role role) {
    this.role = role;
  }  

  @JsonIgnore
  public Set<Comment> getComments() {
    return this.comments;
  }

  public void setComments(Set<Comment> comments) {
    this.comments = comments;
  }

  @JsonIgnore
  public Set<Model> getUpvotes() {
    return this.upvotes;
  }

  public void setUpvotes(Set<Model> upvotes) {
    this.upvotes = upvotes;
  }

  @JsonIgnore
  public Set<Model> getDownvotes() {
    return this.downvotes;
  }

  public void setDownvotes(Set<Model> downvotes) {
    this.downvotes = downvotes;
  }


}