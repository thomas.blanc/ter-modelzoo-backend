package mozen.utils;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import mozen.business.IUserManager;
import mozen.model.Role;
import mozen.model.User;

public class UserHelper {
  
  public static boolean isAuthor(User author, User userToCheck) {
    if (isAdmin(userToCheck))
      return true;
    return author == userToCheck;
  }

  public static boolean isAdmin(User user) {
    return user.getRole() == Role.ADMIN;
  }

  public static User getCurrentUser(IUserManager userManager) {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();

    if (!(auth instanceof AnonymousAuthenticationToken)) {
      if (auth.getPrincipal() instanceof org.springframework.security.core.userdetails.User) return null;
      String username = (String) auth.getPrincipal();
      return userManager.getUserByUsername(username);
    } else {
      return null;
    }
  }
}