package mozen.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mozen.model.Comment;
import mozen.model.Model;
import mozen.model.Role;
import mozen.model.Tag;
import mozen.model.TagCategory;
import mozen.model.User;
import mozen.repos.ModelRepository;
import mozen.repos.TagRepository;
import mozen.repos.UserRepository;

@Service
public class DatabaseFiller {
  @Autowired
  private ModelRepository modelRepo;

  @Autowired
  private UserRepository userRepo;

  @Autowired
  private TagRepository tagRepo;

  @Autowired
  private BCryptPasswordEncoder bCryptPasswordEncoder;

  @EventListener(ContextRefreshedEvent.class)
  public void onApplicationEvent(ContextRefreshedEvent event) {
    // event.getApplicationContext().getBean(DatabaseFiller.class).fillDB();
    event.getApplicationContext().getBean(DatabaseFiller.class).fillDBBasic();
  }

  @Transactional
  public void addAdmin() {
    User admin = new User();
    admin.setEmail("admin@admin.admin");
    admin.setPassword(bCryptPasswordEncoder.encode("1234"));
    admin.setUsername("admin");
    admin.setRole(Role.ADMIN);

    try {
      userRepo.save(admin);
      System.err.println("[DB FILLER] Admin added");
    } catch (Exception e) {
      System.err.println("[DB FILLER] Admin already present");
    }    
  }

  public void fillDBBasic() {
    addAdmin();
    basicTags();
  }

  @Transactional
  public void fillDB() {
    System.err.println("[DB FILLER] Begin");
    User u1 = new User();
    u1.setEmail("user1@email.com");
    u1.setPassword(bCryptPasswordEncoder.encode("1234"));
    u1.setUsername("user 1");
    u1.setRole(Role.DEFAULT);

    User u2 = new User();
    u2.setEmail("user2@email.com");
    u2.setPassword(bCryptPasswordEncoder.encode("1234"));
    u2.setUsername("user 2");
    u2.setRole(Role.DEFAULT);

    Model m1 = new Model();
    m1.setAuthor(u1);
    m1.setName("Model Testo");
    m1.setShortDescription("Short description for model testo");
    m1.setLongDescription("# README \n model testo");
    m1.setVotes(0);
    m1.setParameterCount(10);
    m1.setPerformanceIndex(1.5);
    m1.setPerformance(98.5);
    m1.setPerformanceUnit("accuracy");
    m1.setAdded(new Date());
    m1.setLastModified(new Date());
    m1.setTags(tagLoader());

    Set<Comment> coms = new HashSet<Comment>();
    Comment c1 = new Comment();
    c1.setAuthor(u1);
    c1.setContent("Commentaire 1 de user 1 sur testo");
    c1.setModel(m1);
    c1.setAdded(new Date());

    Comment c2 = new Comment();
    c2.setAuthor(u1);
    c2.setContent("Commentaire 2 de user 1 sur testo");
    c2.setModel(m1);
    c2.setAdded(new Date());

    Comment c3 = new Comment();
    c3.setAuthor(u2);
    c3.setContent("Commentaire de user 2 sur testo");
    c3.setModel(m1);
    c3.setAdded(new Date());

    coms.add(c1);
    coms.add(c2);
    coms.add(c3);

    m1.setComments(coms);

    userRepo.save(u1);
    userRepo.save(u2);

    System.err.println("[DB FILLER] Users added");

    addAdmin();

    modelRepo.save(m1);
    System.err.println("[DB FILLER] Model added");
  }

  Set<Tag> tagLoader() {
    Set<Tag> tags = new HashSet<Tag>();

    Resource resource = new ClassPathResource("tags.txt");

    try {
      BufferedReader br = new BufferedReader(new InputStreamReader(resource.getInputStream()));
      String line;
      while ((line = br.readLine()) != null) {
        System.out.println("[DB FILLER] adding category "+line);
        TagCategory category = new TagCategory();
        category.setName(line);
        line = br.readLine();
        while (!line.equals("#")) {
          System.out.println("[DB FILLER] adding tag "+line+" to "+category.getName());
          Tag tag = new Tag();
          tag.setCategory(category);
          tag.setName(line);
          
          tags.add(tag);

          line = br.readLine();
        }
      }
      System.err.println("[DB FILLER] Tags added");
    } catch (IOException e) {
      System.err.println("[DB FILLER] Tags failed");
    }   

    return tags;
  }

  public void basicTags() {
    try {
      TagCategory category_task = new TagCategory();
      category_task.setName("Task");

      Tag tag = new Tag();
      tag.setCategory(category_task);
      tag.setName("Classification");
      tagRepo.save(tag);


      TagCategory category_arch = new TagCategory();
      category_arch.setName("Architecture");

      Tag tag2 = new Tag();
      tag2.setCategory(category_arch);
      tag2.setName("VGG19");
      tagRepo.save(tag2);


      TagCategory category_dataset = new TagCategory();
      category_dataset.setName("Dataset");

      Tag tag3 = new Tag();
      tag3.setCategory(category_dataset);
      tag3.setName("Cifar10");
      tagRepo.save(tag3);

      System.err.println("[DB FILLER] Basic tags added");
    } catch (Exception e) {
      System.err.println(e);
      System.err.println("[DB FILLER] Basic tags already present");
    }
  }

}