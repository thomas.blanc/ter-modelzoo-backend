package mozen.utils;

import java.security.Key;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collection;
import java.util.Date;

import javax.crypto.spec.SecretKeySpec;

import org.springframework.security.core.GrantedAuthority;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JwtUtils {
  public static Key generateKey() {
    String keyString = "LePetitBonhommeEnMousse";
    Key key = new SecretKeySpec(keyString.getBytes(), 0, keyString.getBytes().length, "DES");
    return key;
  }

  public static String generateToken(String username, Collection<GrantedAuthority> roles) {
    String role = roles.toArray()[0].toString();
    return Jwts.builder()
      .setSubject(username)
      .claim("username", username)
      .claim("role", role)
      .setIssuedAt(new Date())
      .setExpiration(toDate(LocalDateTime.now().plusDays(1L)))
      .signWith(SignatureAlgorithm.HS512, generateKey())
      .compact();
  }

  private static Date toDate(LocalDateTime localDateTime) {
    return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
  }
}