package mozen.utils;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.springframework.util.DigestUtils;
import org.springframework.web.multipart.MultipartFile;

public class Md5Utils {
  public static String getMD5(MultipartFile file) {
    try {
      InputStream inputStream =  new BufferedInputStream(file.getInputStream());
      String digest = DigestUtils.md5DigestAsHex(inputStream);
      return digest;
    } catch (IOException e) {
      e.printStackTrace();
      return null;
    }    
  }
}