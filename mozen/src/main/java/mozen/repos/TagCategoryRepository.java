package mozen.repos;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import mozen.model.TagCategory;

public interface TagCategoryRepository extends CrudRepository<TagCategory, Long>{
  List<TagCategory> findAll();
  List<TagCategory> findByOrderByNameAsc();
}