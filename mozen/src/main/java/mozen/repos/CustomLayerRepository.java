package mozen.repos;

import org.springframework.data.repository.CrudRepository;

import mozen.model.CustomLayer;

public interface CustomLayerRepository extends CrudRepository<CustomLayer, Long>{
  
}