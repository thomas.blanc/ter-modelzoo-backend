package mozen.repos;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import mozen.model.User;

public interface UserRepository extends PagingAndSortingRepository<User, Long> {
  User findByUsername(String username);
  List<User> findAll();
}