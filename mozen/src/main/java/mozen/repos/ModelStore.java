package mozen.repos;

import org.springframework.content.commons.repository.ContentStore;

import mozen.model.Model;

public interface ModelStore extends ContentStore<Model, String>{
  
}