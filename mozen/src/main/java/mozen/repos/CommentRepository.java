package mozen.repos;

import java.util.Collection;

import org.springframework.data.repository.CrudRepository;

import mozen.model.Comment;
import mozen.model.Model;
import mozen.model.User;

public interface CommentRepository extends CrudRepository<Comment, Long>{
  Collection<Comment> findAll();
  Collection<Comment> findByModel(Model model);
  Collection<Comment> findByAuthor(User author);
}