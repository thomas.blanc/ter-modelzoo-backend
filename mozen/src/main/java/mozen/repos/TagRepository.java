package mozen.repos;

import java.util.Collection;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import mozen.model.Tag;

public interface TagRepository extends CrudRepository<Tag, Long>{
  Tag findByName(String name);
  List<Tag> findByNameIn(Collection<String> name);
}