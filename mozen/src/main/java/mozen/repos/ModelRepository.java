package mozen.repos;

import java.util.Collection;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import mozen.model.Model;
import mozen.model.Tag;

public interface ModelRepository extends PagingAndSortingRepository<Model, Long> {
  Collection<Model> findAll();
  Page<Model> findByNameContainingIgnoreCase(String name, Pageable pageable);

  Page<Model> findDistinctByNameContainingIgnoreCaseAndTagsIn(String name, Collection<Tag> tags, Pageable pageable);

  // @Query("SELECT m FROM Model m WHERE m.name LIKE :name AND m.tags CONTAINS (SELECT t FROM Tag WHERE t.name IN :tags)") <- enfer absolu
  // Page<Model> findByNameContainingIgnoreCaseTags(@Param("name") String name, @Param("tags") Collection<String> tags, Pageable pageable);
}