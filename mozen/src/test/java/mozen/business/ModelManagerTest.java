package mozen.business;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import mozen.messages.ModelMessage;
import mozen.model.Model;
import mozen.model.User;

@SpringBootTest
public class ModelManagerTest {
  
  @Autowired
  ModelManager modelManager;

  @Test
  @Transactional
  void addModelTest () {
    ModelMessage message = new ModelMessage();
    message.setName("testmodel");
    message.setShortDescription("this is short desc for testmodel");
    message.setLongDescription("this is long desc for testmodel");
    message.setPerformance(98.9);
    message.setPerformanceUnit("test unit");
    message.setParameterCount(84654);

    User user = new User();
    user.setUsername("test");
    user.setEmail("test@test.com");
    user.setUsername("1234");

    try {
      Model model = modelManager.addModel(message, user);

      assertEquals(message.getName(), model.getName());
      assertEquals(message.getShortDescription(), model.getShortDescription());
      assertEquals(message.getLongDescription(), model.getLongDescription());
      assertEquals(message.getParameterCount(), model.getParameterCount());
      assertEquals(message.getPerformanceUnit(), model.getPerformanceUnit());
      assertEquals(message.getPerformance(), model.getPerformance());
      assertEquals(user.getUsername(), model.getAuthor().getUsername());
      // check tags missing
    } catch (Exception e) {
      assertTrue(false);
    }
  }

  @Test
  @Transactional
  void getModelTest () {
    ModelMessage message = new ModelMessage();
    message.setName("testmodel");
    message.setShortDescription("this is short desc for testmodel");
    message.setLongDescription("this is long desc for testmodel");
    message.setPerformance(98.9);
    message.setPerformanceUnit("test unit");
    message.setParameterCount(84654);

    User user = new User();
    user.setUsername("test");
    user.setEmail("test@test.com");
    user.setUsername("1234");
    
    Model model;
    try { model = modelManager.addModel(message, user);} catch (Exception e) {assertTrue(false); return;}

    Model modelFind = modelManager.getModel(model.getId());
    assertEquals(model, modelFind);
  }

  @Test
  void getModelNotPresentTest () {
    Model modelFind = modelManager.getModel(368436);

    assertNull(modelFind);
  }

  @Test
  @Transactional
  void removeModelTest () {
    ModelMessage message = new ModelMessage();
    message.setName("testmodel");
    message.setShortDescription("this is short desc for testmodel");
    message.setLongDescription("this is long desc for testmodel");
    message.setPerformance(98.9);
    message.setPerformanceUnit("test unit");
    message.setParameterCount(84654);

    User user = new User();
    user.setUsername("test");
    user.setEmail("test@test.com");
    user.setUsername("1234");
    
    Model model;
    try { model = modelManager.addModel(message, user);} catch (Exception e) {assertTrue(false); return;}

    try {
      modelManager.removeModel(model.getId(), user);
    } catch (Exception e) {
      assertTrue(false);
    }

    try {
      Model modelFind = modelManager.getModel(model.getId());
      assertNull(modelFind);
    } catch (Exception e) {
      assertTrue(false);
    }
  }
  
}