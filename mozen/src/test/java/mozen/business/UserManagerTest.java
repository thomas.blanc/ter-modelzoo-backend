package mozen.business;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import mozen.messages.SignupMessage;
import mozen.messages.UserUpdateMessage;
import mozen.model.User;

@SpringBootTest
public class UserManagerTest {
  @Autowired
  UserManager userManager;

  @Test
  @Transactional
  void addUserTest() {
    SignupMessage message = new SignupMessage();
    message.setEmail("test@test.com");
    message.setUsername("testuser");
    message.setPassword("1234");

    User user = userManager.addUser(message);

    assertEquals(message.getEmail(), user.getEmail());
    assertEquals(message.getUsername(), user.getUsername());
    assertNotEquals(message.getPassword(), user.getPassword());
  }

  @Test
  @Transactional
  void getUserTest() {
    User user = userManager.addUser(new SignupMessage("testuser", "1234", "test@test.com"));

    User userFind = userManager.getUser(user.getId());

    assertEquals(user, userFind);
  }

  @Test
  @Transactional
  void getUserByUsernameTest() {
    User user = userManager.addUser(new SignupMessage("testuser", "1234", "test@test.com"));

    User userFind = userManager.getUserByUsername(user.getUsername());

    assertEquals(user, userFind);
  }

  @Test
  @Transactional
  void updateUserTest() {
    User user = userManager.addUser(new SignupMessage("testuser", "1234", "test@test.com"));

    UserUpdateMessage message = new UserUpdateMessage();
    message.setId(user.getId());
    message.setUsername("newtestuser");
    message.setEmail("newtest@test.com");

    try {
      userManager.updateUser(user, message);
    } catch (Exception e) {
      assertTrue(false);
    }

    User userFind = userManager.getUser(user.getId());
    assertEquals(message.getUsername(), userFind.getUsername());
    assertEquals(message.getEmail(), userFind.getEmail());
    assertEquals(message.getUsername(), userFind.getUsername());
  }

  @Test
  @Transactional
  void updateUserNotRightTest() {
    User user = userManager.addUser(new SignupMessage("testuser", "1234", "test@test.com"));
    User user2 = userManager.addUser(new SignupMessage("testuser2", "1234", "test2@test.com"));

    UserUpdateMessage message = new UserUpdateMessage();
    message.setId(user.getId());
    message.setUsername("newtestuser");
    message.setEmail("newtest@test.com");

    try {
      userManager.updateUser(user2, message);
      assertTrue(false);
    } catch (Exception e) {
      assertTrue(true);
      User userFind = userManager.getUser(user.getId());
      assertEquals(user.getUsername(), userFind.getUsername());
      assertEquals(user.getEmail(), userFind.getEmail());
      assertEquals(user.getUsername(), userFind.getUsername());
    }
  }

  @Test
  @Transactional
  void removeUserTest () {
    User user = userManager.addUser(new SignupMessage("testuser", "1234", "test@test.com"));
    Long id = user.getId();

    userManager.removeUser(user, id);

    User userFind = userManager.getUser(id);
    assertNull(userFind);
  }

  @Test
  @Transactional
  void removeUserWrongTest () {
    User user = userManager.addUser(new SignupMessage("testuser", "1234", "test@test.com"));
    User user2 = userManager.addUser(new SignupMessage("testuser2", "1234", "test2@test.com"));
    Long id = user.getId();
    
    userManager.removeUser(user2, id);

    User userFind = userManager.getUser(id);
    assertEquals(user, userFind);
  }
}