git clone https://gitlab.lis-lab.fr/thomas.blanc/ter-modelzoo-frontend.git
cd ter-modelzoo-frontend
npm install
npm run build
mv dist/* ../mozen/src/main/resources/public/
cd ../mozen
mvn package
cd ../
cp mozen/target/mozen-0.0.1-SNAPSHOT.jar mozen.jar
docker build . -t gltron/mozen
docker-compose up -d
