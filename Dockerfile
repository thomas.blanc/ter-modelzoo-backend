FROM alpine:edge
MAINTAINER gltron
RUN apk --update add openjdk8-jre
COPY mozen.jar /home/mozen.jar
CMD ["java","-jar","/home/mozen.jar"]
EXPOSE 8181
